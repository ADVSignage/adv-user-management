module.exports = function(mongoose,DBUri,MongoDBStore,app,session,config) {

    mongoose.connect(DBUri, {  useNewUrlParser: true,useUnifiedTopology: true,useCreateIndex:true,useFindAndModify:false })
        .then(() => {
            var admin = new mongoose.mongo.Admin(mongoose.connection.db);
            admin.buildInfo(function (err, info) {
                console.log("Connected to "+info.version+" MongoDB.")
            });
        }).catch(err => console.error("Could not connect to MongoDB..."+err));

    //USING SESSIONS STORE MONGO
    var sessionStore = new MongoDBStore({
        uri: DBUri,
        collection: 'sessions'
    });

    app.use(session({
        secret: config["session-secret"],
        store: sessionStore,
        cookie: { path: '/', httpOnly: true, secure: false, maxAge: config['cookie-maxAge'], sameSite: true }, // expire session cookie in 60 min or 3600 seconds
        resave:true,
        saveUninitialized: false,
        rolling:true
    }));

}