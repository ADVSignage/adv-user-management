const ROUTES = {
    login : {
        GET : '/login',
        POST : '/login'
    },
    logout : {
        GET : '/logout'
    },
    forgot : {
        GET : '/forgot',
        POST : '/forgot'
    },
    reset : {
        GET : '/reset',
        POST : '/reset'
    },
    profile : {
        GET : '/profile',
        POST : '/profile'
    }
}


module.exports = ROUTES;