//RENDERER
const es6Renderer = require('express-es6-template-engine')

// SERVER, DB , PARSER
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

//Session--
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);


module.exports = {
    es6Renderer , bodyParser , mongoose , session , MongoDBStore
};