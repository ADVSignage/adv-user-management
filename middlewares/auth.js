const ROUTES = require('./../utility/routes.utility');

module.exports.auth = function(req,res,next) {
    if(req.session.user) next();
    else return res.redirect(ROUTES.login.GET);
}

module.exports.notAuth = function(req,res,next) {
    if(!req.session.user) next();
    else return res.redirect(ROUTES.profile.GET);
}