const { notAuth } = require("../middlewares/auth");
const { User, validateLogin } = require("../models/user.model");
const ROUTES = require("../utility/routes.utility");
const bcrypt = require("bcrypt");

module.exports = function(router,callback) {


    router.get(ROUTES.login.GET,notAuth,(req,res) => { callback(req,res) });

    router.post(ROUTES.login.POST,notAuth,async (req,res) => {
        var {email,password,loc} = req.body;

        const { error } = validateLogin({email:email,password:password});
        if (error) return res.status(200).send({status:false,message:'Error during validation of fields ' + error});

        email = req.body.email.toLowerCase();

        let user = await User.findOne({email:email});

        if(user)
        {
            if(password && bcrypt.compareSync(password,user.password)) 
            {
                var sessionStoringUser = Object.assign({}, user._doc);
                delete sessionStoringUser.password;
                req.session.user = sessionStoringUser;
                if(loc) return res.json({status:true,message:loc});
                else return res.json({status:true,message:ROUTES.profile.GET});
            } else {
                req.session.user = false;
                return res.json({status:false,message:'Invalid credentials'});
            }
        } else {
            req.session.user = false;
            return res.json({status:false,message:'Invalid credentials'});
        }
    });

    return router;
}