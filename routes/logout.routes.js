const { notAuth, auth } = require("../middlewares/auth");
const ROUTES = require("../utility/routes.utility");


module.exports = function(router,callback) {


    router.get(ROUTES.logout.GET,auth,(req,res,next) => {    
        req.session.destroy(function(err) {
            if(err) {
                console.log(err);
                return next(err);
            } else {
                return callback(req,res);
            }
        });
    });


    return router;
}