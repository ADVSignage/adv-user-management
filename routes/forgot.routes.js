const { notAuth, auth } = require("../middlewares/auth");
const ROUTES = require("../utility/routes.utility");


module.exports = function(router,callback) {


    router.get(ROUTES.forgot.GET,notAuth,(req,res) => { callback(req,res) });

    router.post(ROUTES.forgot.POST,notAuth,(req,res) => {
        return res.json({status:false,message:'Forgot Post'});
    });

    return router;
}