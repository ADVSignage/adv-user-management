const { notAuth, auth } = require("../middlewares/auth");
const ROUTES = require("../utility/routes.utility");


module.exports = function(router,callback) {


    router.get(ROUTES.reset.GET,notAuth,(req,res) => { callback(req,res) });

    router.post(ROUTES.reset.POST,notAuth,(req,res) => {
        return res.json({status:false,message:'Reset Post'});
    });

    return router;
}