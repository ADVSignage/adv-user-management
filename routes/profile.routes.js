const { notAuth, auth } = require("../middlewares/auth");
const ROUTES = require("../utility/routes.utility");


module.exports = function(router,callback) {


    router.get(ROUTES.profile.GET,auth,(req,res) => { callback(req,res) });

    router.post(ROUTES.profile.POST,auth,(req,res) => {
        return res.json({status:false,message:'User Profile post'});
    });

    return router;
}