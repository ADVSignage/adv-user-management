const Joi = require('joi');
const mongoose = require('mongoose');

//user schema
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: false,
    minlength: 3,
    maxlength: 50
  },
  email: {
    type: String,
    unique: true,
    required: true,
    minlength: 5,
    maxlength: 75
  },
  verified: {
    type: Boolean,
    default: false
  },
  verifiedDate: {
    type:Date
  },
  verification_code : {
    type: String,
    unique: true,
    required: false
  },
  password: {
    type: String,
    required: false,
    minlength: 3,
    maxlength: 255
  },
  enabled:{
    type:Boolean,
    required:false,
    default:true
  }
});

const User = mongoose.model('User', UserSchema);

//function to validate user 
function validateUser(data) {
    const schema = Joi.object({
        name: Joi.string().min(3).max(50).required,
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(3).max(255).required()
    });
        
    const validation = schema.validate(data);
    return validation;
}

function validatePassword(data) {
    const schema = Joi.object({
        newPassword: Joi.string().min(3).max(255).required()
    });
        
    const validation = schema.validate(data);
    return validation;
}
function validateLogin(data) {
    const schema = Joi.object({
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(3).max(255).required()
    });
        
    const validation = schema.validate(data);
    return validation;
}

exports.User = User; 
exports.validate = validateUser;
exports.validatePassword = validatePassword;
exports.validateLogin = validateLogin;