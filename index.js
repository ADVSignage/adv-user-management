const { es6Renderer ,  bodyParser , mongoose , session , MongoDBStore } = require('./config');
const { notAuth, auth } = require('./middlewares/auth');
const { User, validate } = require('./models/user.model');
const ROUTES = require('./utility/routes.utility');
const bcrypt = require("bcrypt");

/**
 * 
 * @param app Your express app instance
 * @param DBUri Youy MongoDB DB uri
 * @param config Your configurations { "cookie-maxAge" : 3600000 , "session-secret" : 'privatekey' , "bcrypt-hashing" : 16 }
 */
module.exports.init = function ( app , DBUri , config = {
    "cookie-maxAge" : 600000,
    "session-secret" : 'myprivatesessionkey',
    "bcrypt-hashing" : 16
}) {
    if(config["cookie-maxAge"] && config["cookie-maxAge"] >= 600000) {} 
    else config["cookie-maxAge"] = 600000;

    if(config["session-secret"]) {} 
    else config["session-secret"] = 'myprivatesessionkey';

    if(config["bcrypt-hashing"] && config["bcrypt-hashing"] > 8) {} 
    else config["bcrypt-hashing"] = 16;

    global.CONFIG = config;

    require('./db-connection')(mongoose,DBUri,MongoDBStore,app,session,config);
    
    app.use(bodyParser.urlencoded({ extended: false })); 
    app.use(bodyParser.json({ extended: true, limit: '50mb' }));

    app.use(async (req,res,next) => {
        if(req.local) {}
        else req.local = {};

        if(req.partial) {}
        else req.partial = {};
        
        if(req.session && req.session.user) req.local.user = req.session.user;

        next();
    });
}

/**
 * 
 * @param {*} app 
 * @param {*} router 
 * @param options E nables the routes for user profile,forgot password and reset password
 */
module.exports.initRoutes = function(app,router,options = {
    enableUserProfile: false,
    enableForgotPassword: false,
    enableResetPassword: false
},callbacks = {
    getLogin  : function(req,res) { return res.send('getLogin')  },
    getLogout : function(req,res) { return res.send('getLogout') },
    getForgot : function(req,res) { return res.send('getForgot') },
    getReset  : function(req,res) { return res.send('getReset')  },
    getProfile: function(req,res) { return res.send('getProfile')}
}) {
    app.use(require('./routes/login.routes')(router,callbacks.getLogin));
    app.use(require('./routes/logout.routes')(router,callbacks.getLogout));

    if(options.enableUserProfile) app.use(require('./routes/profile.routes')(router,callbacks.getProfile));

    if(options.enableForgotPassword) app.use(require('./routes/forgot.routes')(router,callbacks.getForgot));

    if(options.enableResetPassword) app.use(require('./routes/reset.routes')(router,callbacks.getReset));
};


module.exports.advUserManagementROUTES = ROUTES;

module.exports.middlewares = { auth , notAuth };

/**
 * 
 * @param user User object to put in DB {email:email,password:password}
 */
module.exports.newUser = async function(user)
{
    const {error} = validate({email:user.email,password:user.password});
    if(error) return {status:false,data:error.details[0].message};
    
    user.password = await bcrypt.hash(user.password, global.CONFIG["bcrypt-hashing"]);

    var _user = new User(user);
    return await _user.save().then(doc => {
        return {status:true,data:doc};
    }).catch(e => {
        return {status:false,data:e.errmsg};
    })
}


module.exports.updateUser = async function(user)
{
    const {error} = validate({email:user.email,password:user.password});
    if(error) return {status:false,data:error.details[0].message};

    user.password = await bcrypt.hash(user.password, global.CONFIG["bcrypt-hashing"]);


    await User.updateOne({email:user.email},{$set:user},(err,raw) => {
        if(err) return {status:false,data:JSON.stringify(err)};
        else return {status:true,data:raw};
    });
}

module.exports.cryptMessage = async function(message = null)
{
    message = (message != null) ? message : 'default';
    const crypted = await bcrypt.hash(message, global.CONFIG["bcrypt-hashing"]);
    return crypted;
}